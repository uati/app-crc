//package br.com.itau.archetype.appcrc.v1.api.IT;
//
//import br.com.itau.archetype.appcrc.config.IntegrationTest;
//import br.com.itau.archetype.appcrc.v1.api.controller.EnderecoRestController;
//import com.github.tomakehurst.wiremock.WireMockServer;
//import com.jayway.restassured.RestAssured;
//import com.jayway.restassured.http.ContentType;
//import org.apache.http.HttpStatus;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import static com.github.tomakehurst.wiremock.client.WireMock.*;
//import static org.hamcrest.Matchers.*;
//
//@RunWith(SpringRunner.class)
//@IntegrationTest
//public class EnderecoRestApiIT {
//
//    @LocalServerPort
//    private int serverPort;
//
//    WireMockServer wireMockServer;
//
//    @BeforeClass
//    public static void init() {
//
//    }
//
//    @Before
//    public void setUp() {
//        RestAssured.port = this.serverPort;
//        wireMockServer = new WireMockServer(3000);
//        wireMockServer.start();
//        setupStub();
//    }
//
//    public void setupStub() {
//        wireMockServer.stubFor(get(urlEqualTo("/pedidos"))
//                .willReturn(aResponse().withHeader("Content-Type", "application/json")
//                        .withStatus(200)
//                        .withBodyFile("pedidos.json")));
//    }
//
//    @Test
//    public void test_get_should_request_pedidos() {
//        RestAssured.given()
//                .contentType(ContentType.JSON)
//                .when()
//                .get(EnderecoRestController.URL)
//                .then()
//                .statusCode(HttpStatus.SC_OK)
//                .assertThat()
//                .body("size", is(greaterThan(0)))
//                .body("[0]", hasKey("id"))
//                .body("[0]", hasKey("numero"));
//    }
//
//
//}
