package br.com.itau.archetype.appcrc.v1.service;

import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate.LABEL_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME;
import static br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate.LABEL_CUSTOMER_INFO_COMPLETE;
import static org.junit.Assert.assertNotNull;


@RunWith(MockitoJUnitRunner.class)
public class CustomerInfoServiceTests {

    @InjectMocks
    private CustomerInfoService customerInfoService;

    @Mock
    private CustomerInfoRepository customerInfoRepository;

    private CustomerInfoEntity customerInfoEntity;
    private CustomerInfoEntity customerInfoEntityWithID;

    @BeforeClass
    public static void init() {
        FixtureFactoryLoader.loadTemplates(CustomerInfoFixtureTemplate.class.getPackage().getName());
    }

    @Before
    public void setUp() {
        customerInfoEntity = Fixture.from(CustomerInfoEntity.class)
                .gimme(LABEL_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME);

        customerInfoEntityWithID = Fixture.from(CustomerInfoEntity.class)
                .gimme(LABEL_CUSTOMER_INFO_COMPLETE);
    }

    @Test
    public void test_save() {
        Mockito.when(customerInfoRepository.save(customerInfoEntity)).thenReturn(customerInfoEntityWithID);
        CustomerInfoEntity saved = customerInfoService.save(customerInfoEntity);
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

}
