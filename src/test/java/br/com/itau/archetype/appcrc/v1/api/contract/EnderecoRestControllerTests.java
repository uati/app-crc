package br.com.itau.archetype.appcrc.v1.api.contract;

import br.com.itau.archetype.appcrc.config.IntegrationTest;
import br.com.itau.archetype.appcrc.v1.api.controller.CustomerInfoRestController;
import br.com.itau.archetype.appcrc.v1.api.controller.EnderecoRestController;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@IntegrationTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = {"br.com.itau.crc.poc:app-endereco:+:stubs:3000"},
        stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class EnderecoRestControllerTests {

    @LocalServerPort
    private int serverPort;

    @Before
    public void setUp() {
        RestAssured.port = this.serverPort;
    }

    @Test
    public void test_get_enderecos() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(EnderecoRestController.URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .assertThat()
                .statusCode(is(equalTo(200)));
    }


}
