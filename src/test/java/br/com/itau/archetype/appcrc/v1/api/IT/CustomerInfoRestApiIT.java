package br.com.itau.archetype.appcrc.v1.api.IT;

import br.com.itau.archetype.appcrc.config.IntegrationTest;
import br.com.itau.archetype.appcrc.v1.api.controller.CustomerInfoRestController;
import br.com.itau.archetype.appcrc.v1.api.domain.CustomerInfo;
import br.com.itau.archetype.appcrc.v1.api.mapper.CustomerInfoMapper;
import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate;
import br.com.itau.archetype.appcrc.v1.service.CustomerInfoRepository;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@IntegrationTest
public class CustomerInfoRestApiIT {


    @LocalServerPort
    private int serverPort;

    @Autowired
    private CustomerInfoRepository repository;

    @Autowired
    private CustomerInfoMapper mapper;

    private CustomerInfo customerInfo;

    @BeforeClass
    public static void init() {
        FixtureFactoryLoader.loadTemplates(CustomerInfoFixtureTemplate.class.getPackage().getName());
    }

    @Before
    public void setUp() {
        RestAssured.port = this.serverPort;
        this.repository.deleteAll();
        this.customerInfo = Fixture.from(CustomerInfo.class).gimme(CustomerInfoFixtureTemplate.DOMAIN_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME);
        //Assert.assertThat(this.customerInfo.getId(), isEmptyOrNullString());
        //this.repository.save(this.customerInfo);
        //assertThat(this.product.getId(), not(isEmptyOrNullString()));
    }

    @Test
    public void test_post_should_request_one_customer_without_id_and_after_request_should_to_exist_one_cusgtomer_on_repository() {
        //this.repository.deleteAll();
        Assert.assertNull(customerInfo.getId());
        RestAssured.given()
                .body(customerInfo)
                .contentType(ContentType.JSON)
                .when()
                .post(CustomerInfoRestController.URL)
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .assertThat()
                .body("$", hasKey("id"))
                .body("$", hasKey("name"))
                .body("name", equalTo(customerInfo.getName()));

        List<CustomerInfoEntity> all = (List<CustomerInfoEntity>) this.repository.findAll();
        Assert.assertThat(all.size(), Is.is(greaterThan(0)));
    }

    @Test
    public void test_get_should_request_customers() {
        test_post_should_request_one_customer_without_id_and_after_request_should_to_exist_one_cusgtomer_on_repository();
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(CustomerInfoRestController.URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .assertThat()
                .body("size", is(greaterThan(0)))
                .body("[0]", hasKey("id"))
                .body("[0]", hasKey("name"))
                .and()
                .body("[0].id", is(greaterThan(0)))
                .body("[0].name", equalTo(customerInfo.getName()));
    }


}
