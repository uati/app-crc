package br.com.itau.archetype.appcrc.v1.api.controller;

import br.com.itau.archetype.appcrc.v1.api.domain.CustomerInfo;
import br.com.itau.archetype.appcrc.v1.api.mapper.CustomerInfoMapper;
import br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate;
import br.com.itau.archetype.appcrc.v1.service.CustomerInfoService;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate.DOMAIN_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerInfoRestController.class)
public class CustomerInfoRestControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerInfoService customerInfoService;

    @MockBean
    private CustomerInfoMapper customerInfoMapper;

    @Autowired
    private ObjectMapper objectMapper;

    private String jsonCustomer;
    private CustomerInfo customerInfo;


    @BeforeClass
    public static void init() {
        FixtureFactoryLoader.loadTemplates(CustomerInfoFixtureTemplate.class.getPackage().getName());
    }

    @Before
    public void setUp() {
        customerInfo = Fixture.from(CustomerInfo.class)
                .gimme(DOMAIN_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME);
        jsonCustomer = convertToJson(customerInfo);
    }

    @Test
    public void test_getAll() throws Exception {
        String json = mockMvc.perform(get(CustomerInfoRestController.URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse().getContentAsString();
        System.out.println(json);
    }

    @Test
    public void test_save() throws Exception {
        mockMvc.perform(post(CustomerInfoRestController.URL)
                .content(jsonCustomer)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
                //.andExpect(content().json(jsonCustomer));
    }

    private String convertToJson(CustomerInfo customerInfo) {
        try {
            return objectMapper.writeValueAsString(customerInfo);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
