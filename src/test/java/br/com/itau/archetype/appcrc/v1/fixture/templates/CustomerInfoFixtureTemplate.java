package br.com.itau.archetype.appcrc.v1.fixture.templates;

import br.com.itau.archetype.appcrc.v1.api.domain.CustomerInfo;
import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class CustomerInfoFixtureTemplate implements TemplateLoader {

    public static final String LABEL_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME = "LABEL_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME";
    public static final String LABEL_CUSTOMER_INFO_COMPLETE = "LABEL_CUSTOMER_INFO_COMPLETE";

    public static final String DOMAIN_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME = "DOMAIN_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME";

    public class CustomerInfoFields {
        public static final String ID = "id";
        public static final String NAME = "name";
    }

    @Override
    public void load() {
        Fixture.of(CustomerInfoEntity.class).addTemplate(LABEL_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME, new Rule() {
            {
                add(CustomerInfoFields.ID, null);
                add(CustomerInfoFields.NAME, "Peter Quill");
            }
        });
        Fixture.of(CustomerInfoEntity.class).addTemplate(LABEL_CUSTOMER_INFO_COMPLETE, new Rule() {
            {
                add(CustomerInfoFields.ID, 2L);
                add(CustomerInfoFields.NAME, "Peter Quill");
            }
        });

        // DOMAIN
        Fixture.of(CustomerInfo.class).addTemplate(DOMAIN_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME, new Rule() {
            {
                //add(CustomerInfoFields.ID, null);
                add(CustomerInfoFields.NAME, "Peter Quill");
            }
        });
    }
}
