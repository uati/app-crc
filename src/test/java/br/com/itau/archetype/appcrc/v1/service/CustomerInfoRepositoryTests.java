package br.com.itau.archetype.appcrc.v1.service;

import br.com.itau.archetype.appcrc.config.TestSupport;
import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import br.com.itau.archetype.appcrc.v1.fixture.templates.CustomerInfoFixtureTemplate;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@TestSupport
@DataJpaTest
@RunWith(SpringRunner.class)
public class CustomerInfoRepositoryTests {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CustomerInfoRepository customerInfoRepository;

    private CustomerInfoEntity customerInfoEntity;

    @BeforeClass
    public static void init() {
        FixtureFactoryLoader.loadTemplates(CustomerInfoFixtureTemplate.class.getPackage().getName());
    }

    @Before
    public void setUp() {
        this.customerInfoEntity = Fixture.from(CustomerInfoEntity.class)
                .gimme(CustomerInfoFixtureTemplate.LABEL_CUSTOMER_INFO_WITHOUT_ID_AND_WITH_NAME);
    }

    @Test
    public void test_findByName() {
        Iterable<CustomerInfoEntity> customers = this.customerInfoRepository.findByName("Mary Jane");
        assertNotNull(customers);
    }
}
