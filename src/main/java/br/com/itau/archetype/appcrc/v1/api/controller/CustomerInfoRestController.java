package br.com.itau.archetype.appcrc.v1.api.controller;

import br.com.itau.archetype.appcrc.v1.api.domain.CustomerInfo;
import br.com.itau.archetype.appcrc.v1.api.mapper.CustomerInfoMapper;
import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import br.com.itau.archetype.appcrc.v1.service.CustomerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(CustomerInfoRestController.URL)
public class CustomerInfoRestController {

    public static final String URL = "/api/v1/customers";

    @Autowired
    private CustomerInfoService customerInfoService;

    @Autowired
    private CustomerInfoMapper customerInfoMapper;

    @GetMapping
    public Iterable<CustomerInfo> getAll() {
        Iterable<CustomerInfoEntity> all = this.customerInfoService.getAll();
        return this.customerInfoMapper.mapToDomain(all);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerInfo save(@Valid @RequestBody CustomerInfo customerInfo) {
        CustomerInfoEntity customerInfoEntity = this.customerInfoMapper.mapToEntity(customerInfo);
        CustomerInfoEntity saved = this.customerInfoService.save(customerInfoEntity);
        return this.customerInfoMapper.mapToDomain(saved);
    }

}
