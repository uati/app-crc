package br.com.itau.archetype.appcrc.v1.service;

import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerInfoRepository extends CrudRepository<CustomerInfoEntity, Long> {

    Iterable<CustomerInfoEntity> findByName(String name);
}
