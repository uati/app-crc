package br.com.itau.archetype.appcrc.v1.api.controller;

import br.com.itau.archetype.appcrc.v1.client.domain.Endereco;
import br.com.itau.archetype.appcrc.v1.service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(EnderecoRestController.URL)
public class EnderecoRestController {

    public static final String URL = "/api/v1/enderecos";

    @Autowired
    private EnderecoService enderecoService;

    @GetMapping
    public List<Endereco> getAll() {
        return this.enderecoService.getAll();
    }

}
