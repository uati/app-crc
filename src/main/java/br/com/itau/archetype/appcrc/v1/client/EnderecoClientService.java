package br.com.itau.archetype.appcrc.v1.client;

import br.com.itau.archetype.appcrc.v1.client.domain.Endereco;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "enderecoClientService", url = "${endereco.client.service.url}")
public interface EnderecoClientService {

    @GetMapping
    public List<Endereco> getAll();

}
