package br.com.itau.archetype.appcrc.v1.service;

import br.com.itau.archetype.appcrc.v1.client.EnderecoClientService;
import br.com.itau.archetype.appcrc.v1.client.domain.Endereco;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoService {

    @Autowired
    private EnderecoClientService enderecoClientService;

    public List<Endereco> getAll() {
        return this.enderecoClientService.getAll();
    }

}
