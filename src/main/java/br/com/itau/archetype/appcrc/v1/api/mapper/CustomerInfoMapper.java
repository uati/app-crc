package br.com.itau.archetype.appcrc.v1.api.mapper;

import br.com.itau.archetype.appcrc.v1.api.domain.CustomerInfo;
import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerInfoMapper {

    @Autowired
    private Mapper mapper;

    public Iterable<CustomerInfo> mapToDomain(Iterable<CustomerInfoEntity> entities) {
        List<CustomerInfo> listResource = new ArrayList<>();
        for (CustomerInfoEntity e : entities) {
            listResource.add(mapToDomain(e));
        }
        return listResource;
    }

    public CustomerInfo mapToDomain(CustomerInfoEntity entity) {
        return this.mapper.map(entity, CustomerInfo.class);
    }

    public CustomerInfoEntity mapToEntity(CustomerInfo domain) {
        return this.mapper.map(domain, CustomerInfoEntity.class);
    }

}
