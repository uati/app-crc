package br.com.itau.archetype.appcrc.v1.service;

import br.com.itau.archetype.appcrc.v1.entity.CustomerInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerInfoService {

    @Autowired
    private CustomerInfoRepository customerInfoRepository;

    public CustomerInfoEntity save(CustomerInfoEntity customerInfoEntity) {
        return this.customerInfoRepository.save(customerInfoEntity);
    }

    public Iterable<CustomerInfoEntity> getAll() {
        return this.customerInfoRepository.findAll();
    }
}
