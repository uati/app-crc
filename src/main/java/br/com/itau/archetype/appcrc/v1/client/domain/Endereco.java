package br.com.itau.archetype.appcrc.v1.client.domain;

import lombok.Data;

@Data
public class Endereco {

    private String id;
    private Integer numero;
    private String logradouro;

}
