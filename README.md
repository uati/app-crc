# App CRC

- Spring Boot   

* Web
* JPA
* Devtools
* MySQL
* H2
* Lombok
* Fixture Factory
* Dozer
* Swagger
* Actuator
* Undertow
#### Plugins
* Sonar
* Jacoco

### Gerando um novo Archetype app-crc-archetype    
```bash
mvn archetype:create-from-project
```
- Em `target > generated-sources > archetype` é gerado seu Archetype.
- Edite os arquivos abaixo:    
    * `pom.xml`   
    * `src/main/resources/META-INF/maven/archetype-metadata.xml`
    * `src/main/resources/archetype-resources/README.md`   
- Exclua os arquivos gerados a mais e o que achar necessário.   


#### Instalando o Archetype Local
```bash
cd target/generated-sources/archetype
mvn clean install
```

#### Criando uma nova aplicação
```bash 
  mvn archetype:generate \
      -DarchetypeGroupId=br.com.itau.archetype \
      -DarchetypeArtifactId=app-crc-archetype \
      -DarchetypeVersion=0.0.1-SNAPSHOT \
      -DgroupId=br.com.itau.crc.re2 \
      -Dpackage=br.com.itau.crc.re2.myapp \
      -DartifactId=my-app 
      -Dversion=1.0.0-SNAPSHOT  
```